FROM docker/dev-environments-default
RUN sudo apt-get update && sudo apt-get install -y python3-venv python3-pip make binutils libz-dev git curl devscripts apt-utils python3-cachecontrol
RUN sudo ln -s /usr/bin/python3 /usr/bin/python
RUN sudo ln -s /usr/bin/pip3 /usr/bin/pip
